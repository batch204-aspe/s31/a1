// 1. What directive is used by Node.js in loading the modules it needs?
	
	/*
		Answer

			► HTTP module

		Correction : require()

	*/


// 2. What Node.js module contains a method for server creation?
	
	/*
		Answer

			► http.createServer() method

	*/


// 3. What is the method of the http object responsible for creating a server using Node.js?

	/*
		Answer

			► listen(3000/4000) Method 

	*/



// 4. What method of the response object allows us to set status codes and content types?
	
	/*
		Answer

			► response.writeHead(200, {'Content-Type': 'text/plain'});

	*/


// 5. Where will console.log() output its contents when run in Node.js?
	
	/*
		Answer

			► Current TERMINAL directory
	*/

// 6. What property of the request object contains the address's endpoint?

	/*
		Answer

			► response.end('');

		Correction : .url

	*/